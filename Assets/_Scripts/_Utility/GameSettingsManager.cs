﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingsManager : Singleton<GameSettingsManager> {

	[SerializeField]
	public PlayerSettings playerSettings;





	[System.Serializable]
	public class PlayerSettings
	{
		[Header ("Player Attributes")]
		public float speedGrounded;
		public float accelerationGrounded;
		public float speedAir;
		public float accelerationAir;
		public float playerGravity;
		public float jumpForce;
		public float maxJumpHoldTime;
		public float minJumpHoldTime;

		[Header ("Player Controls")]
		public KeyCode key_Jump;
	}




}
