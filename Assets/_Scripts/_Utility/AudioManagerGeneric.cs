﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent (typeof (AudioSource))]

public class AudioManagerGeneric : MonoBehaviour {



	AudioSource fxSource;
	[Header ("---Pitch Settings---")]
	[Tooltip ("The lowest pitch to which a sound effect will be randomly pitched.")]
	public float lowPitchRange = .95f;
	[Tooltip ("The highest pitch to which a sound effect will be randomly pitched.")]
	public float highPitchRange = 1.05f;

	[Header (" ")] // Spacing
	[Header ("---Audio Clips and Settings---")]
	public List<AudioObject> audioObjects = new List<AudioObject>();

	public enum Loop {loop, noLoop};

	float initialVolume;
	bool muted = false;

	void Awake () {

		fxSource = GetComponent<AudioSource>();

	}

	void Start ()
	{
		initialVolume = fxSource.volume;
	}

	// Method to call to play a sound once
	public void FindAndPlay (string name) {
		if (!muted)
		{

			for (int i = 0; i < audioObjects.Count; i++) {

				if (audioObjects[i].clipName.ToLower() == name.ToLower()) {
					PlaySound (audioObjects[i], Loop.noLoop);
					return;
				}

			}
		}
	}

	// Method to call to play a sound looped
	public void FindAndPlayLooped (string name) {
		if (!muted)
		{
			for (int i = 0; i < audioObjects.Count; i++) {

				if (audioObjects[i].clipName.ToLower() == name.ToLower()) {
					PlaySound (audioObjects[i], Loop.loop);
					return;
				}

			}
		}
	}


	public void PlaySound (AudioObject audioObject, Loop loopStatus)
	{
		//Set the clip of our efxSource audio source to the clip passed in as a parameter.
		fxSource.clip = audioObject.clip;

		// Pitch modulation
		if (audioObject.modulatePitch == true) {
			float randomPitch = Random.Range(lowPitchRange, highPitchRange);
			fxSource.pitch = randomPitch;
		}

		else {
			fxSource.pitch = 1;
		}


		// Looping
		if (loopStatus == Loop.loop) {
			fxSource.loop = true;
		}

		else {
			fxSource.loop = false;
		}

		fxSource.volume = audioObject.volume;

		//Play the clip.
		fxSource.Play ();
	}


	public void StopPlaying () {

		fxSource.Stop ();
		fxSource.loop = false;

	}

	public void ToggleMute ()
	{
		if (!muted)
		{
			muted = true;
			fxSource.volume = 0;
		}

		else
		{
			muted = false;
			fxSource.volume = initialVolume;
		}
	}

	#region AudioObject Struct

	[System.Serializable]
	public struct AudioObject {

		public string clipName;
		public AudioClip clip;
		public float volume;
		public bool modulatePitch;



		public AudioObject (string _clipName, AudioClip _clip, float _volume, bool _modulatePitch) {
			clipName = _clipName;
			clip = _clip;
			volume = _volume;
			modulatePitch = _modulatePitch;

		}

	}

	#endregion

}
