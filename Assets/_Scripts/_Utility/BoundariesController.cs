﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoundariesController : Singleton<BoundariesController> {

	public List<GameObject> boundaries;

	public enum BoundaryMode {LeftRight, TopBottom, BotLeftTopRight, BotRightTopLeft};
	[Header ("Settings")]
	public BoundaryMode boundaryMode;
	public float boundaryPushbackDuration;
	public AnimationCurve boundaryPushbackAnimCurve;
	public float boundaryPushbackAmount;

	[Header ("Prefabs")]
	public GameObject boundaryPrefab;

	LevelData currentLevelData;

	bool expanding = false;

	bool expansionStarted = false;

	Coroutine expansionCoroutine;
	Coroutine boundaryPushbackCoroutine;

	float currentSpeed;

	void OnEnable ()
	{
		EventManager.Instance.e_PushBackBoundary += PushBackBoundaryByAmount;
		EventManager.Instance.e_PushBackBoundaryToOrigin += PushBackBoundaryToOrigin;
		EventManager.Instance.e_PauseBoundaryExpanding += PauseExpanding;
		EventManager.Instance.e_UnpauseBoundaryExpanding += UnpauseExpanding;
	}

	void OnDisable ()
	{
		EventManager.Instance.e_PushBackBoundary -= PushBackBoundaryByAmount;
		EventManager.Instance.e_PushBackBoundaryToOrigin -= PushBackBoundaryToOrigin;
		EventManager.Instance.e_PauseBoundaryExpanding -= PauseExpanding;
		EventManager.Instance.e_UnpauseBoundaryExpanding -= UnpauseExpanding;
	}


	void Start ()
	{
		currentLevelData = LevelManager.Instance.levelData[LevelManager.Instance.currentLevel];
		boundaryMode = currentLevelData.initialBoundaryMode;

		CreateBoundaries();

		for (int i = 0; i < transform.childCount; i++)
		{
			boundaries.Add (transform.GetChild(i).gameObject);
		}

		SetInitialBoundarySizes(boundaryMode);
		SetInitialBoundaryPositions(boundaryMode);

		expansionCoroutine = StartCoroutine (ExpandBoundaries());
	}

	void Update ()
	{
		currentSpeed = currentLevelData.initialBoundarySpeed;
	}

	void CreateBoundaries ()
	{
		if (boundaryMode == BoundaryMode.LeftRight)
		{
			for (int i = 0; i < 2; i++)
			{
				GameObject bound = Instantiate(boundaryPrefab, transform, true) as GameObject;
				bound.transform.SetParent (transform);

				// Reverse the texture scrolling direction for the second barrier
				if (i == 1)
				{
					bound.transform.GetChild(0).GetComponent<TextureScroller>().xSpeed *= -1;
				}
			}
		}

		else
		{
			Debug.Log ("OTHER BOUNDARY MODES NOT IMPLEMENTED YET, USE LEFT-RIGHT!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}


	void SetInitialBoundarySizes (BoundaryMode mode)
	{
		float camHeight = 2f * Camera.main.orthographicSize * 100;

		for (int i = 0; i < boundaries.Count; i++)
		{
			boundaries[i].transform.localScale = new Vector2 (currentLevelData.initialBoundaryWidth, 10000);
		}


	}

	void SetInitialBoundaryPositions (BoundaryMode mode)
	{
		if (mode == BoundaryMode.LeftRight)
		{
			for (int i = 0; i < boundaries.Count; i++)
			{
				boundaries[i].transform.position = new Vector3 (Camera.main.ViewportToWorldPoint(new Vector2(0+i, 0.5f)).x, 0, -1);
			}
		}
	}

	IEnumerator ExpandBoundaries ()
	{
		float expansionStartTime = 0;
		while (!expanding && expansionStartTime < currentLevelData.expansionDelay)
		{
			expansionStartTime += Time.deltaTime;
			yield return 0;
		}

		expansionStarted = true;
		expanding = true;

		while (expansionStarted)
		{
			if (expanding && boundaryMode == BoundaryMode.LeftRight)
			{
				boundaries[0].transform.localScale = new Vector2 (boundaries[0].transform.localScale.x + currentSpeed, 10000);
				boundaries[1].transform.localScale = new Vector2 (boundaries[1].transform.localScale.x + currentSpeed, 10000);
			}

			yield return 0;
		}
		
	}

	void PushBackBoundaryByAmount ()
	{
		if (boundaryPushbackCoroutine == null)
		{
			boundaryPushbackCoroutine = StartCoroutine (AnimateBoundaryPushBack(boundaryPushbackAmount, true));
		}
	}

	void PushBackBoundaryToOrigin ()
	{
		if (boundaryPushbackCoroutine != null)
		{
			StopCoroutine (boundaryPushbackCoroutine);
			boundaryPushbackCoroutine = StartCoroutine (AnimateBoundaryPushBack(boundaries[0].transform.localScale.x, false));
		}

		else
		{
			boundaryPushbackCoroutine = StartCoroutine (AnimateBoundaryPushBack(boundaries[0].transform.localScale.x, false));
		}
	}

	IEnumerator AnimateBoundaryPushBack (float amount, bool resumeExpanding)
	{
		expanding = false;
		float time = 0;

		Vector2[] boundaryInitialScales = new Vector2[2];

		boundaryInitialScales[0] = boundaries[0].transform.localScale;
		boundaryInitialScales[1] = boundaries[1].transform.localScale;

		while (time < boundaryPushbackDuration)
		{
			float durationPercent = time / boundaryPushbackDuration;
			if (boundaryMode == BoundaryMode.LeftRight)
			{
				boundaries[0].transform.localScale = new Vector2 (Mathf.Lerp (boundaryInitialScales[0].x, boundaryInitialScales[1].x - amount, boundaryPushbackAnimCurve.Evaluate (durationPercent)),
																			  10000);
				boundaries[1].transform.localScale = new Vector2 (Mathf.Lerp (boundaryInitialScales[1].x, boundaryInitialScales[1].x - amount, boundaryPushbackAnimCurve.Evaluate (durationPercent)),
																			  10000);
			}
			time += Time.deltaTime;
			yield return 0;
		}
			
		boundaryPushbackCoroutine = null;
		 
		expanding = resumeExpanding;
	}

	void PauseExpanding ()
	{
		expanding = false;
	}

	void UnpauseExpanding ()
	{
		if (boundaryPushbackCoroutine != null)
		{
			StopCoroutine (boundaryPushbackCoroutine);
			expanding = true;
		}

		else
		{
			expanding = true;
		}

	}

}
