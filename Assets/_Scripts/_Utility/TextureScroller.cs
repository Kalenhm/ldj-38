﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureScroller : MonoBehaviour {

	bool scrolling = true;

	public float xSpeed;
	public float ySpeed;

	Material mat;


	void Start ()
	{
		mat = GetComponent<MeshRenderer>().material;
	}

	void  Update ()
	{
		if (scrolling)
		{
			mat.mainTextureOffset = new Vector2 (mat.mainTextureOffset.x + xSpeed * Time.deltaTime, mat.mainTextureOffset.y + ySpeed * Time.deltaTime);
		}

	}



}
