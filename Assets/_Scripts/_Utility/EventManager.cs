﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager : Singleton<EventManager> {


	#region Player Events
	public event Action e_PlayerEnterBarrier;
	public event Action e_PlayerExitBarrier;
	public event Action e_PushBackBoundary;
	public event Action e_PushBackBoundaryToOrigin;
	public event Action e_PauseBoundaryExpanding;
	public event Action e_UnpauseBoundaryExpanding;

	public event Action<GameObject> e_SetCheckpoint;
	public event Action e_ReturnToCheckpoint;
	public event Action e_EnterCheckpoint;
	public event Action e_LeaveCheckpoint;

	public event Action e_PlayerDied;
	public event Action e_PlayerRespawned;
	#endregion




	public void PlayerEnterBarrier ()
	{
		if (e_PlayerEnterBarrier != null) { e_PlayerEnterBarrier(); }
	}

	public void PlayerExitBarrier ()
	{
		if (e_PlayerExitBarrier != null) { e_PlayerExitBarrier(); }
	}

	public void PushBackBoundary ()
	{
		if (e_PushBackBoundary != null) { e_PushBackBoundary();}
	}

	public void PushBackBoundaryToOrigin ()
	{
		if (e_PushBackBoundaryToOrigin != null) { e_PushBackBoundaryToOrigin(); }
	}

	public void PauseBoundaryExpanding ()
	{
		if (e_PauseBoundaryExpanding != null) { e_PauseBoundaryExpanding (); }
	}

	public void UnpauseBoundaryExpanding ()
	{
		if (e_UnpauseBoundaryExpanding != null) { e_UnpauseBoundaryExpanding (); }
	}

	public void SetCheckpoint (GameObject check)
	{
		if (e_SetCheckpoint != null) { e_SetCheckpoint (check); }
	}

	public void ReturnToCheckpoint ()
	{
		if (e_ReturnToCheckpoint != null) { e_ReturnToCheckpoint (); }
	}

	public void EnterCheckpoint ()
	{
		if (e_EnterCheckpoint != null) { e_EnterCheckpoint (); }
	}

	public void LeaveCheckPoint ()
	{
		if (e_LeaveCheckpoint != null) { e_LeaveCheckpoint (); }
	}

	public void PlayerDied ()
	{
		if (e_PlayerDied != null) { e_PlayerDied (); }
	}

	public void PlayerRespawned ()
	{
		if (e_PlayerRespawned != null) { e_PlayerRespawned (); }
	}

}
