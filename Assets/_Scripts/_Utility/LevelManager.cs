﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton <LevelManager> {
	public int currentLevel = 0;
	public LevelData[] levelData;
}

[System.Serializable]
public class LevelData
{
	[Header ("Scroll Speed")]
	public float initialScrollSpeed;
	public float maxScrollSpeed;

	[Header("Boundaries")]
	public BoundariesController.BoundaryMode initialBoundaryMode;
	public float initialBoundarySpeed;
	public float maxBoundarySpeed;
	public float initialBoundaryWidth;
	public float maxBoundaryWidth;
	public float expansionDelay;

	[Header ("Health")]
	public float boundaryDPS;
//	public float regenRatePerSecond;
	public float regenTime;
	public float regenDelay;
	public AnimationCurve regenCurve;
}
