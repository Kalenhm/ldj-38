﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#region Required Components
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider2D))]
#endregion

public class PlayerController : MonoBehaviour {


	public GameSettingsManager.PlayerSettings playerSettings;

	Vector2 directionalInput;
	Vector3 velocity;
	float velocitySmoothingX;

	float jumpTimer = 0f;

	CollisionData colData;

	Rigidbody2D rb;


	void Awake()
	{
		playerSettings = GameSettingsManager.Instance.playerSettings;
	}

	void Start ()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void Update ()
	{
		GenerateCollisionData();
		velocity.y = -playerSettings.playerGravity;
		HandleInput();
		CalculateVelocity();

		Move();
	}



	void HandleInput()
	{

		directionalInput = new Vector2 (Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));



		if (Input.GetKey(GameSettingsManager.Instance.playerSettings.key_Jump))
		{
			OnJumpInputDown();
		}

		if (Input.GetKeyUp (GameSettingsManager.Instance.playerSettings.key_Jump))
		{
			OnJumpInputUp();
		}


	}

	void Move ()
	{
		transform.Translate (new Vector2 (velocity.x * Time.deltaTime, velocity.y * Time.deltaTime));
	}


	#region Jumping

	void OnJumpInputDown ()
	{
		if (jumpTimer < playerSettings.maxJumpHoldTime)
		{
			velocity.y = playerSettings.jumpForce;
			jumpTimer += Time.deltaTime;
		}
		else
		{
			jumpTimer = 0;
		}


	}

	void OnJumpInputUp ()
	{

	}

	#endregion

	void CalculateVelocity ()
	{
		float targetVelocityX = directionalInput.x * playerSettings.speedGrounded;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocitySmoothingX, playerSettings.accelerationGrounded);
	}
		

	void GenerateCollisionData ()
	{
		colData = new CollisionData();
	}
		


}


public class CollisionData
{
	bool above, below;
	bool left, right;


}

