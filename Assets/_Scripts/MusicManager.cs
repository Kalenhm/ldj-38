﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

public class MusicManager : Singleton <MusicManager> {

	EventManager eventManager;

	Coroutine lowPassFadeCoroutine;

	public float lowPassDefaultFrequency;
	public float lowPassEnabledFrequency;
	public float lowPassFadeTime;

	AudioSource source;
	public AudioMixer mixer;

	public AudioSource blackHoleFX;
	PlayerStatsManager playerStats;

	void Awake ()
	{
		eventManager = EventManager.Instance;
	}

	void Start ()
	{
		source = GetComponent<AudioSource>();
		mixer.SetFloat ("LowPassCutoffFreq", lowPassDefaultFrequency);
		playerStats = PlayerStatsManager.Instance;

	}


	void OnEnable ()
	{
		eventManager.e_PlayerEnterBarrier += MusicLowPassEnable;
		eventManager.e_PlayerExitBarrier += MusicLowPassDisable;
	}

	void OnDisable ()
	{
		eventManager.e_PlayerEnterBarrier -= MusicLowPassEnable;
		eventManager.e_PlayerExitBarrier -= MusicLowPassDisable;
	}

	void Update ()
	{
		blackHoleFX.volume = 1 - (playerStats.health / playerStats.maxHealth);
	}


	void MusicLowPassEnable ()
	{
		if (lowPassFadeCoroutine == null)
		{
			lowPassFadeCoroutine = StartCoroutine(LowPassFade (true));
		}

		else
		{
			StopCoroutine (lowPassFadeCoroutine);
			lowPassFadeCoroutine = StartCoroutine(LowPassFade (true));
		}
	}

	void MusicLowPassDisable ()
	{
		if (lowPassFadeCoroutine == null)
		{
			lowPassFadeCoroutine = StartCoroutine(LowPassFade (false));
		}

		else
		{
			StopCoroutine (lowPassFadeCoroutine);
			lowPassFadeCoroutine = StartCoroutine(LowPassFade (false));
		}
	}


	IEnumerator LowPassFade (bool enabled)
	{
		float time = 0;

		if (enabled)
		{
			while (time < lowPassFadeTime)
			{
				mixer.SetFloat ("LowPassCutoffFreq", Mathf.Lerp (lowPassDefaultFrequency, lowPassEnabledFrequency, time / lowPassFadeTime));
				time += Time.deltaTime;
				yield return 0;
			}
		}

		else
		{
			while (time < lowPassFadeTime)
			{
				mixer.SetFloat ("LowPassCutoffFreq", Mathf.Lerp (lowPassEnabledFrequency, lowPassDefaultFrequency, time / lowPassFadeTime));
				time += Time.deltaTime;
				yield return 0;
			}
		}

	}


}
