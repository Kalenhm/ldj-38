﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

	public int id;
	public GameObject particle;
	public GameObject activate;
	ParticleSystem activateParticle;
	bool activated = false;
	AudioManagerGeneric amg;

	void Start ()
	{
		activateParticle = activate.GetComponent<ParticleSystem>();
		amg = GetComponent<AudioManagerGeneric>();
	}


	public void ActivateParticle()
	{
		if (!activated)
		{
			activateParticle.Play();
			amg.FindAndPlay("Checkpoint");
		}
	}

}
