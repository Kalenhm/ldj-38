﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterController : MonoBehaviour {
	
	public Vector2 exitPointLocal;
	Vector2 exitPointGlobal;
	
	
	void Start () {
		exitPointGlobal.x = exitPointLocal.x + transform.position.x;
		exitPointGlobal.y = exitPointLocal.y + transform.position.y;
	}
	
	void OnTriggerEnter2D (Collider2D collider)
	{
		Transform playerTransform = collider.gameObject.transform;
		Player player = playerTransform.GetComponent<Player> ();
		if ((collider.gameObject.tag == "Player") && (player.hasTeleported == false))
		{
			playerTransform.SetPositionAndRotation (new Vector3 (exitPointGlobal.x, exitPointGlobal.y, 0), playerTransform.rotation);
			
			if (player.teleportCoroutine == null)
			{
				player.teleportCoroutine = StartCoroutine (player.TeleportCD ());
			}
			else
			{
				player.StopCoroutine (player.teleportCoroutine);
				player.teleportCoroutine = StartCoroutine (player.TeleportCD ());
			}
		}
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.blue;
		float size = .3f;

		Vector2 exitGlobalPos = (Application.isPlaying)?exitPointGlobal : exitPointLocal + (Vector2) transform.position;
		Gizmos.DrawLine(exitGlobalPos - Vector2.up * size, exitGlobalPos + Vector2.up * size);
		Gizmos.DrawLine(exitGlobalPos - Vector2.left * size, exitGlobalPos + Vector2.left * size);
	}

}
