﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LineRenderer))]

public class Switch : MonoBehaviour {
	
	public GameObject terrainPiece;
	public bool triggeredByPlayer = true;
	public bool startDeactivated;
	public bool isRing = false;
	
	public float singleRotation;
	
	public Vector3 singleMovePoint;
	Vector3 moveGlobalWaypoint;
	Vector3 initialGlobalWaypoint;
	Quaternion initialRotation;
	
	bool hasActivated = false;
	
	PlatformController platform;
	BoostController boost;

	LineRenderer line;

	AudioManagerGeneric amg;

	Color origColor;

	SpriteRenderer sr;
	
	
	
	
	void Start ()
	{
		platform = terrainPiece.GetComponent<PlatformController> ();
		boost = terrainPiece.GetComponentInChildren<BoostController> ();
		
		initialGlobalWaypoint = terrainPiece.transform.position;
		initialRotation = terrainPiece.transform.rotation;

//		line = GetComponent<LineRenderer>();
//		UpdateLineRenderers();
		amg = GetComponent<AudioManagerGeneric>();
		sr = GetComponent<SpriteRenderer>();

		origColor = sr.color;
		
		if (startDeactivated)
		{
			Reset();
		}
	}
	
	public void Reset ()
	{
		terrainPiece.transform.SetPositionAndRotation (initialGlobalWaypoint, initialRotation);	

		if (platform != null && !isRing)
		{
			platform.Reset ();
		}

		else if (platform != null && isRing && startDeactivated && !hasActivated)
		{
			platform.enabled = false;
		}

		if (boost != null)
		{
			for (int i = 0; i < terrainPiece.transform.childCount; i++)
			{
				terrainPiece.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
		hasActivated = false;
		sr.color = origColor;
	}
	
	void OnTriggerEnter2D (Collider2D collider)
	{
		if (triggeredByPlayer && collider.gameObject.tag == "Player")
		{
			Activate ();
		}
	}
	
	public void Activate ()
	{
		if (!hasActivated)
		{
			terrainPiece.transform.Rotate (new Vector3 (0, 0, singleRotation));	

			terrainPiece.transform.Rotate (-initialRotation.eulerAngles);
			terrainPiece.transform.Translate (singleMovePoint);
			terrainPiece.transform.Rotate (initialRotation.eulerAngles);
			hasActivated = true;

			amg.FindAndPlay ("Switch");
			sr.color = new Color (1, 1, 1, 0.75f);
		}

		if (platform != null)
		{
			platform.Actiwate ();
			hasActivated = true;
		}
		if (boost != null)
		{
			for (int i = 0; i < terrainPiece.transform.childCount; i++)
			{
				terrainPiece.transform.GetChild (i).gameObject.SetActive (true);
			}

		}




//		UpdateLineRenderers();
	}


//	void UpdateLineRenderers ()
//	{
//		line.SetPosition (0, transform.position);
//		line.SetPosition (1, terrainPiece.transform.position);
//	}

}
