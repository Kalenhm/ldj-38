﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CheckpointManager : Singleton <CheckpointManager> {


	public GameObject currentCheckpoint;
	public Checkpoint currentCheckpointScript;
	public int checkpointID;

	public GameObject player;

	void OnEnable ()
	{
		EventManager.Instance.e_SetCheckpoint += SetCheckpoint;
		EventManager.Instance.e_ReturnToCheckpoint += ReturnToCheckpoint;
	}

	void OnDisable ()
	{
		EventManager.Instance.e_SetCheckpoint -= SetCheckpoint;
		EventManager.Instance.e_ReturnToCheckpoint -= ReturnToCheckpoint;
	}


	void Start ()
	{
		player = FindObjectOfType<Player>().gameObject;
	}


	public void SetCheckpoint (GameObject check)
	{
		int checkID = check.GetComponent<Checkpoint>().id;

		if (currentCheckpoint == null)
		{
			currentCheckpoint = check;
			currentCheckpointScript = currentCheckpoint.GetComponent<Checkpoint>();
			checkpointID = currentCheckpointScript.id;
			currentCheckpointScript.ActivateParticle();
		}

		else if (currentCheckpointScript.id < checkID)
		{
			currentCheckpoint = check;
			currentCheckpointScript = currentCheckpoint.GetComponent<Checkpoint>();
			checkpointID = currentCheckpointScript.id;
			currentCheckpointScript.ActivateParticle();
		}

	}

	public void ReturnToCheckpoint ()
	{
		player.transform.position = new Vector2 (currentCheckpoint.transform.position.x, currentCheckpoint.transform.position.y + 1);
	}


}
