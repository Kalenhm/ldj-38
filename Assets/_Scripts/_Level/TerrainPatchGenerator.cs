﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainPatchGenerator : MonoBehaviour {

	public int patchHalfWidth, patchHalfHeight;
	public GameObject[] groundPrefab = new GameObject[5];
	public GameObject player;
	
	//Generate a patch of random terrain at point on game world
	void GeneratePatch (Vector2 originPoint)
	{
		//Instantiate and set properties if necessary for random number of obstacle objects, intersperse them randomly within bounds of patch
		int numObstacles = Random.Range (1, 5);
		
		for (int i = 0; i < numObstacles; i++)
		{
			float xVal = (float) Random.Range (originPoint.x - patchHalfWidth, originPoint.x + patchHalfWidth);
			float yVal = (float) Random.Range (originPoint.y - patchHalfHeight, originPoint.y + patchHalfHeight);
			GameObject currPrefab = groundPrefab [Random.Range (0, groundPrefab.Length)];
			
			GameObject terrain = (GameObject) GameObject.Instantiate (currPrefab, 
																	  new Vector2 (xVal, yVal), 
																	  currPrefab.transform.rotation);
		}
	}
	
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.LeftControl)) 
		{
			float playerX = player.transform.position.x;
			float playerY = player.transform.position.y;
			GeneratePatch (new Vector2 (playerX + 2*patchHalfWidth, playerY)); //right
			GeneratePatch (new Vector2 (playerX + 2*patchHalfWidth, playerY + 2*patchHalfHeight)); //up-right
			GeneratePatch (new Vector2 (playerX, playerY + 2*patchHalfHeight)); //up
			GeneratePatch (new Vector2 (playerX - 2*patchHalfWidth, playerY)); //left
			GeneratePatch (new Vector2 (playerX - 2*patchHalfWidth, playerY + 2*patchHalfHeight)); //up-left
			
		}
	}
}
