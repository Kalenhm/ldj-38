﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostController : MonoBehaviour {

	public Vector2 velocity;

	
	
	void OnTriggerEnter2D (Collider2D collider)
	{

		if (enabled && collider.gameObject.tag == "Player")
		{
			Player player = collider.gameObject.transform.GetComponent<Player>();
			
			player.BoostVerticalVelocity (velocity.y);
			player.BoostHorizontalVelocity (velocity.x);
			
			if (player.boostCoroutine == null)
			{
				player.boostCoroutine = StartCoroutine (player.BoostTimer ());
			}
			else
			{
				player.StopCoroutine (player.boostCoroutine);
				player.boostCoroutine = StartCoroutine (player.BoostTimer ());
			}
		}
	}
}
