﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour {

	[Header ("Blur Panel")]
	public GameObject blurPanel;
	public float blurPanelStartBlur;
	public float blurPanelFinalBlur;
	public Color blurPanelStartTint;
	public Color blurPanelFinalTint;
	public float blurFadeTime;
	Image blurImage;
	Coroutine fadeCoroutine;

	[Header ("Objects")]
	public float elementFadeTime;
	public GameObject title;
	public GameObject button;
	public Text titleText;
	public Image buttonImage;
	float buttonImageStartAlpha;
	public Text buttonText;
	public AnimationCurve fadeAnimCurve;

	AudioManagerGeneric amg;

	public Player player;

	// Use this for initialization
	void Start () {
		// TODO: DISABLE THE PLAYER

		blurImage = blurPanel.GetComponent<Image>();

		blurImage.material.SetFloat("_Size", blurPanelStartBlur);
		blurImage.material.SetVector ("_Color", blurPanelStartTint);
		buttonImageStartAlpha = buttonImage.color.a;
		amg = GetComponent<AudioManagerGeneric>();

		player = FindObjectOfType<Player>();
		player.enabled = false;
		// TODO: FIX THIS IT IS GARBAGE
		LevelManager.Instance.levelData[0].initialBoundarySpeed = 0;
	}


	public void StartButton()
	{
		if (fadeCoroutine == null)
		{
			fadeCoroutine = StartCoroutine (FadeBlurOut());
			amg.FindAndPlay("Start");
		}

	}



	IEnumerator FadeBlurOut ()
	{

		float time = 0;

		while (time < elementFadeTime)
		{
			buttonImage.color = new Color (buttonImage.color.r, buttonImage.color.g, buttonImage.color.b, Mathf.Lerp (buttonImageStartAlpha, 0, fadeAnimCurve.Evaluate (time / elementFadeTime)));
			buttonText.color = new Color (buttonText.color.r, buttonText.color.g, buttonText.color.b, Mathf.Lerp (1, 0, fadeAnimCurve.Evaluate (time / elementFadeTime)));
			time += Time.deltaTime;
			yield return 0;
		}
			
		time = 0;

		while (time < elementFadeTime)
		{
			titleText.color = new Color (titleText.color.r, titleText.color.g, titleText.color.b, Mathf.Lerp (1, 0, fadeAnimCurve.Evaluate (time / elementFadeTime)));
			time += Time.deltaTime;
			yield return 0;
		}

		time = 0;

		while (time < blurFadeTime)
		{
			blurImage.material.SetFloat("_Size", Mathf.Lerp (blurPanelStartBlur, blurPanelFinalBlur, time / blurFadeTime));
			blurImage.material.SetVector ("_Color", new Vector4 (Mathf.Lerp (blurPanelStartTint.r, blurPanelFinalTint.r, time / blurFadeTime), Mathf.Lerp (blurPanelStartTint.g, blurPanelFinalTint.g, time / blurFadeTime), Mathf.Lerp (blurPanelStartTint.b, blurPanelFinalTint.b, time / blurFadeTime), Mathf.Lerp (blurPanelStartTint.a, blurPanelFinalTint.a, time / blurFadeTime)));
			time += Time.deltaTime;
			yield return 0;
		}
			

		FadeDone();
	}

	void FadeDone ()
	{
		this.gameObject.SetActive (false);
		player.enabled = true;
		LevelManager.Instance.levelData[0].initialBoundarySpeed = 0.8f;
	}






}
