﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraEffects : MonoBehaviour {

	[Header ("Basic Settings")]
	public bool effectsEnabled = true;

	[Header ("Grain")]
	public float grainIntensityStart = 0;
	public float grainIntensityGoal;

	[Header ("Grayscale")]
	public float saturationStart = 1;
	public float saturationGoal = 0.25f;

	[Header ("Vignette")]
	public float vignetteStart = 0.1f;
	public float vignetteGoal = 0.36f;

	PostProcessingProfile profile;

	Coroutine fadeCoroutine;

	PlayerStatsManager playerStats;

	float lastFrameHealthPercent;

	void Start ()
	{
		profile = GetComponent<PostProcessingBehaviour>().profile;
		playerStats = PlayerStatsManager.Instance;

		var grain = profile.grain.settings;
		grain.intensity = grainIntensityStart;
		profile.grain.settings = grain;

		var saturation = profile.colorGrading.settings;
		saturation.basic.saturation = saturationStart;
		profile.colorGrading.settings = saturation;

		var vignette = profile.vignette.settings;
		vignette.intensity = vignetteStart;
		profile.vignette.settings = vignette;


		lastFrameHealthPercent = playerStats.health / playerStats.maxHealth;
		fadeCoroutine = StartCoroutine (FadeEffectsBasedOnHealth());
	}



	IEnumerator FadeEffectsBasedOnHealth ()
	{
		while (effectsEnabled)
		{
			float playerHealthPercent = playerStats.health / playerStats.maxHealth;
			float grainTarget = Mathf.Lerp (grainIntensityStart, grainIntensityGoal, 1 - playerHealthPercent);
			float saturationTarget = Mathf.Lerp (saturationStart, saturationGoal, 1 - playerHealthPercent);
			float vignetteTarget = Mathf.Lerp (vignetteStart, vignetteGoal, 1 - playerHealthPercent);

			var grain = profile.grain.settings;
			grain.intensity = grainTarget;
			profile.grain.settings = grain;

			var saturation = profile.colorGrading.settings;
			saturation.basic.saturation = saturationTarget;
			profile.colorGrading.settings = saturation;

			var vignette = profile.vignette.settings;
			vignette.intensity = vignetteTarget;
			profile.vignette.settings = vignette;

			yield return 0;
		}

	}


}
