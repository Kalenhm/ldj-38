﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager> {

	public Image healthBarImage;
	public Text healthBarText;


	void Update ()
	{
		UpdateHealthBar();
	}



	void UpdateHealthBar()
	{
		healthBarImage.fillAmount = PlayerStatsManager.Instance.health / PlayerStatsManager.Instance.maxHealth;
		healthBarText.text = (int)PlayerStatsManager.Instance.health + " / " + PlayerStatsManager.Instance.maxHealth;
	}



}
