﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerDeathParticle : MonoBehaviour {


	ParticleSystem particle;
	GameObject player;

	void Start ()
	{
		particle = GetComponent<ParticleSystem>();
		player = FindObjectOfType<Player>().gameObject;
	}


	void OnEnable ()
	{
		EventManager.Instance.e_PlayerDied += PlayParticleEffect;
	}

	void OnDisable ()
	{
		EventManager.Instance.e_PlayerDied -= PlayParticleEffect;
	}


	void PlayParticleEffect ()
	{
		transform.position = player.transform.position;
		particle.Play();
	}


}
