﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsManager : Singleton<PlayerStatsManager> {

	public float maxHealth;
	public float health;
	public bool isDead = false;

	Coroutine playerDamageCoroutine;
	Coroutine playerRegenCoroutine;

	EventManager eventManager;
	LevelData levelData;


	#region Events
	void OnEnable()
	{
		eventManager = EventManager.Instance;
		eventManager.e_PlayerEnterBarrier += DamagePlayer;
		eventManager.e_PlayerExitBarrier += StopDamagingPlayer;

		eventManager.e_EnterCheckpoint += StartPlayerRegen;
		eventManager.e_LeaveCheckpoint += StopPlayerRegen;

		eventManager.e_PlayerDied += PlayerDied;
		eventManager.e_PlayerRespawned += PlayerRespawn;
	}

	void OnDisable()
	{
		eventManager.e_PlayerEnterBarrier -= DamagePlayer;
		eventManager.e_PlayerExitBarrier -= StopDamagingPlayer;

		eventManager.e_EnterCheckpoint -= StartPlayerRegen;
		eventManager.e_LeaveCheckpoint -= StopPlayerRegen;

		eventManager.e_PlayerDied -= PlayerDied;
		eventManager.e_PlayerRespawned -= PlayerRespawn;
	}
	#endregion

	void Start ()
	{
		if (maxHealth <= 0)
		{
			Debug.Log ("Player Stats Manager: Player's max health cannot be zero!");
			maxHealth = 1;
		}
		health = maxHealth;
		levelData = LevelManager.Instance.levelData[LevelManager.Instance.currentLevel];
	}

	void DamagePlayer ()
	{
		if (playerDamageCoroutine == null)
		{
			playerDamageCoroutine = StartCoroutine (OngoingDamage());	
		}

		else
		{
			StopCoroutine (playerDamageCoroutine);
			playerDamageCoroutine = StartCoroutine (OngoingDamage());	
		}
	}


	IEnumerator OngoingDamage ()
	{

		while (health > 0)
		{
			float damageThisFrame = levelData.boundaryDPS * Time.deltaTime;
			health = health > 0 ? health -= damageThisFrame : health = 0;
			yield return 0;
		}

	}

	void StopDamagingPlayer ()
	{
		if (playerDamageCoroutine != null)
		{
			StopCoroutine (playerDamageCoroutine);
			playerDamageCoroutine = null;
		}
	}

	void StartPlayerRegen()
	{
		if (playerRegenCoroutine == null)
		{
			playerRegenCoroutine = StartCoroutine (PlayerRegen());
		}
	}

	void StopPlayerRegen()
	{
		if (playerRegenCoroutine != null)
		{
			StopCoroutine (playerRegenCoroutine);
			playerRegenCoroutine = null;
		}
	}

	IEnumerator PlayerRegen ()
	{
		yield return new WaitForSeconds (levelData.regenDelay);
		float rtime = 0;
		float startHealth = health;

		while (true)
		{
			if (health >= maxHealth)
			{
				health = maxHealth;
				rtime = 0;
			}
			else
			{
//				health += levelData.regenRatePerSecond * Time.deltaTime;
				health = Mathf.Lerp (startHealth, maxHealth, levelData.regenCurve.Evaluate (rtime / levelData.regenTime));
			}
			rtime += Time.deltaTime;
			yield return 0;
		}
	}

	void PlayerDied ()
	{
		isDead = true;
	}

	void PlayerRespawn ()
	{
		health = maxHealth;
		isDead = false;
	}



}
