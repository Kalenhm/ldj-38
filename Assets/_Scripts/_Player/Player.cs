﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class Player : MonoBehaviour {

	public float maxJumpHeight = 4;
	public float minJumpHeight = 1;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	float moveSpeed = 6;

	public Vector2 wallJumpClimb;
	public Vector2 wallJumpOff;
	public Vector2 wallLeap;
	[HideInInspector]
	public bool doubleJump = false;

	public float wallSlideSpeedMax = 3;
	public float wallStickTime = .25f;
	float timeToWallUnstick;

	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;

	Controller2D controller;

	Vector2 directionalInput;
	bool wallSliding;
	int wallDirX;
	
	public float boostTimer;
	bool isBoosted = false;
	float boostVelocityX;
	float inputXInfluence = 1;
	[HideInInspector]
	public Coroutine boostCoroutine;
	
	public float teleportTimer;
	[HideInInspector]
	public bool hasTeleported = false;
	[HideInInspector]
	public Coroutine teleportCoroutine;

	PlayerEvents playerEvents;

	void Start() {
		controller = GetComponent<Controller2D> ();
		playerEvents = GetComponent<PlayerEvents>();

		gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpHeight);
	}

	void Update() {
		CalculateVelocity ();
		HandleWallSliding ();

		controller.Move (velocity * Time.deltaTime, directionalInput);

		if (controller.collisions.above || controller.collisions.below) {
			if (controller.collisions.slidingDownMaxSlope) {
				velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
			} else {
				velocity.y = 0;
			}
		}
	}

	public void SetDirectionalInput (Vector2 input) {
		directionalInput = input;
	}

	public void OnJumpInputDown() {
		if (wallSliding) {
			if (wallDirX == directionalInput.x) {
				velocity.x = -wallDirX * wallJumpClimb.x;
				velocity.y = wallJumpClimb.y;
			}
			else if (directionalInput.x == 0) {
				velocity.x = -wallDirX * wallJumpOff.x;
				velocity.y = wallJumpOff.y;
			}
			else {
				velocity.x = -wallDirX * wallLeap.x;
				velocity.y = wallLeap.y;
			}
		}
		if (controller.collisions.below) {
			if (controller.collisions.slidingDownMaxSlope) {
				if (directionalInput.x != -Mathf.Sign (controller.collisions.slopeNormal.x)) { // not jumping against max slope
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}
			} else {
				velocity.y = maxJumpVelocity;
			}
		}
		if (doubleJump)
		{
			velocity.y = maxJumpVelocity;
			doubleJump = false;
			playerEvents.DisableDoubleJump();
		}
	}

	public void OnJumpInputUp() {
		if (velocity.y > minJumpVelocity) {
			velocity.y = minJumpVelocity;
		}
	}
		

	void HandleWallSliding() {
		wallDirX = (controller.collisions.left) ? -1 : 1;
		wallSliding = false;
		if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0) {
			wallSliding = true;

			if (velocity.y < -wallSlideSpeedMax) {
				velocity.y = -wallSlideSpeedMax;
			}

			if (timeToWallUnstick > 0) {
				velocityXSmoothing = 0;
				velocity.x = 0;

				if (directionalInput.x != wallDirX && directionalInput.x != 0) {
					timeToWallUnstick -= Time.deltaTime;
				}
				else {
					timeToWallUnstick = wallStickTime;
				}
			}
			else {
				timeToWallUnstick = wallStickTime;
			}

		}

	}

	void CalculateVelocity() {
		float targetVelocityX = (directionalInput.x * moveSpeed) * inputXInfluence;
		if (isBoosted)
		{
			targetVelocityX = boostVelocityX;
		}
		
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
	}
	
	public void BoostVerticalVelocity (float velocityY)
	{
		velocity.y = velocityY;
	}
	
	public void BoostHorizontalVelocity (float velocityX)
	{
		velocity.x = velocityX;
		boostVelocityX = velocityX;
	}
	
	public IEnumerator BoostTimer ()
	{
		float time = 0;
		playerEvents.Boost();

		isBoosted = true;
		while (time < boostTimer)
		{
			inputXInfluence = Mathf.Lerp (0, 1, time / boostTimer);
			time += Time.deltaTime;
			yield return 0;
		}
		isBoosted = false;
		boostCoroutine = null;
	}
	
	public IEnumerator TeleportCD ()
	{
		float time = 0;

		hasTeleported = true;
		while (time < teleportTimer)
		{
			time += Time.deltaTime;
			yield return 0;
		}
		hasTeleported = false;
		teleportCoroutine = null;
	}
	
}
