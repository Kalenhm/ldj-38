﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerEvents : MonoBehaviour {

	EventManager eventManager;
	PlayerStatsManager playerStats;
	[Header ("Prefab Attachment")]
	public GameObject deathParticlePrefab;
	Player player;
	SpriteRenderer sprite;

	public Color defaultColor;
	public Color inBarrierColor;

	float spriteFadeTime = 2f;

	public AudioManagerGeneric boostAudio;
	public AudioManagerGeneric deathAudio;
	AudioManagerGeneric amg;

	public ParticleSystem doubleJumpParticle;


	void OnEnable ()
	{
		eventManager.e_PlayerDied += PlayerDied;
		//eventManager.e_PlayerDied += ResetSwitches;
		eventManager.e_PlayerEnterBarrier += SpriteColorIn;
		eventManager.e_PlayerExitBarrier += SpriteColorOut;		
		eventManager.e_PlayerEnterBarrier += EnableDoubleJump;
		eventManager.e_PlayerExitBarrier += EnableDoubleJump;

	}

	void OnDisable ()
	{
		eventManager.e_PlayerDied -= PlayerDied;
		//eventManager.e_PlayerDied -= ResetSwitches;
		eventManager.e_PlayerEnterBarrier -= SpriteColorIn;
		eventManager.e_PlayerExitBarrier -= SpriteColorOut;		
		eventManager.e_PlayerEnterBarrier -= EnableDoubleJump;
		eventManager.e_PlayerExitBarrier -= EnableDoubleJump;	
	}

	void Awake ()
	{
		eventManager = EventManager.Instance;
	}

	void Start ()
	{
		
		playerStats = PlayerStatsManager.Instance;
		Instantiate (deathParticlePrefab, new Vector3 (0, 0, 0), Quaternion.identity);
		player = GetComponent<Player>();
		sprite = transform.Find("Sprite").GetComponent<SpriteRenderer>();
		amg = GetComponent<AudioManagerGeneric>();
		doubleJumpParticle.Stop();
	}

	void Update ()
	{
		if (playerStats.health <= 0 && !playerStats.isDead)
		{
			eventManager.PlayerDied();
		}
	}




	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Boundary")
		{
			eventManager.PlayerEnterBarrier();
			Time.timeScale = 0.7f;
		}

		if ( col.tag == "Checkpoint")
		{
			eventManager.SetCheckpoint (col.gameObject);
			eventManager.EnterCheckpoint ();
			eventManager.PauseBoundaryExpanding ();
			eventManager.PushBackBoundaryToOrigin();
		}

		if (col.tag == "Killbox")
		{
			playerStats.health = 0;
		}

	}

	void OnTriggerExit2D (Collider2D col)
	{
		if (col.tag == "Boundary")
		{
			eventManager.PlayerExitBarrier();
			Time.timeScale = 1f;
		}

		if (col.tag == "Checkpoint")
		{
			eventManager.LeaveCheckPoint();
			eventManager.UnpauseBoundaryExpanding();
		}
			
	}

	void PlayerDied ()
	{
		player.enabled = false;
		deathAudio.FindAndPlay ("Death");
		StartCoroutine (PlayerDiedFade());
	}


	IEnumerator PlayerDiedFade ()
	{
		float time = 0;

		while (time < spriteFadeTime)
		{
			sprite.color = new Color (sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp (1, 0, time/spriteFadeTime));
			time += Time.deltaTime;
			yield return 0;
		}

		PlayerRespawn();
	}

	void SpriteColorOut ()
	{
		sprite.color = defaultColor;
	}

	void SpriteColorIn ()
	{
		sprite.color = inBarrierColor;
	}

	void PlayerRespawn ()
	{
		eventManager.PlayerRespawned();
		eventManager.ReturnToCheckpoint();
		player.enabled = true;
		sprite.color = new Color (sprite.color.r, sprite.color.g, sprite.color.b, 1);
		
		ResetSwitches ();
	}

	void EnableDoubleJump () 
	{
		player.doubleJump = true;
		doubleJumpParticle.Play();
	}

	public void DisableDoubleJump ()
	{
		doubleJumpParticle.Stop();
	}

	public void Boost ()
	{
		boostAudio.FindAndPlay ("Boost");
	}
	
	void ResetSwitches ()
	{
		Switch[] switches = FindObjectsOfType (typeof(Switch)) as Switch[];
		foreach (Switch _switch in switches)
		{
			_switch.Reset ();
		}
			
	}
	
}
