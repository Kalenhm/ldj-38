﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour {

	public bool activated = false;

	ParticleSystem particle;

	AudioManagerGeneric amg;

	void Start ()
	{
		particle = GetComponent<ParticleSystem>();
		amg = GetComponent<AudioManagerGeneric>();
	}



	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Orb" && !activated)
		{
			Orb orb = col.gameObject.GetComponent<Orb>();
			SpriteRenderer orbSprite = col.gameObject.GetComponent<SpriteRenderer>();
			orb.placed = true;
			col.gameObject.transform.position = transform.position;
			col.gameObject.GetComponent<Rigidbody2D>().simulated = false;
			col.gameObject.GetComponent<CircleCollider2D>().enabled = false;
			orbSprite.color = new Color (orbSprite.color.r, orbSprite.color.g, orbSprite.color.b, 0.25f);

			particle.Stop();
			col.gameObject.GetComponent<ParticleSystem>().Stop();

			RingActivated();
		}
	}


	void RingActivated ()
	{
		activated = true;
		amg.FindAndPlay ("Ring Activate");
		GetComponent<Switch>().Activate();
	}

}
