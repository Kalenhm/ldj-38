﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour {

	Vector2 originalPosition;

	EventManager eventManager;
	Rigidbody2D rb;

	public bool placed = false;

	void Awake ()
	{
		eventManager = EventManager.Instance;
	}

	void OnEnable ()
	{
		eventManager.e_PlayerRespawned += ResetPos;
	}

	void OnDisable ()
	{
		eventManager.e_PlayerRespawned -= ResetPos;
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Killbox")
		{
			ResetPos();
		}
	}

	// Use this for initialization
	void Start () {

		originalPosition = transform.position;
		rb = GetComponent<Rigidbody2D>();	
	}


	void ResetPos ()
	{
		if (!placed)
		{
			rb.velocity = new Vector2 (0, 0);
			transform.position = originalPosition;
		}
	}



}
